import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
{

  path: 'primas', 
  loadChildren: () => import('./modules/liberty-finanzas/liberty-finanzas.module').then(m => m.LibertyFinanzasModule)

}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

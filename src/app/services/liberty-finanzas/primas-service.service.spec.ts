import { TestBed } from '@angular/core/testing';

import { PrimasServiceService } from './primas-service.service';

describe('PrimasServiceService', () => {
  let service: PrimasServiceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PrimasServiceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

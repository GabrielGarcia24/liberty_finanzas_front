import { Injectable } from '@angular/core';
import { HttpClient,HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

export interface BaseResponse{
  data: any;
}

@Injectable({
  providedIn: 'root'
})
export class PrimasServiceService {

  constructor(private http: HttpClient) { }

  public getDataFromFinanzas(dato: String): Observable<BaseResponse> {
    let headers: HttpHeaders = new HttpHeaders().set('Content-Type','application/json;charset=UTF-8')
    return this.http.get<BaseResponse>('http://127.0.0.1:8080/' + 'api/v1/finanzas/' + dato,
    {headers: headers});
  }
}

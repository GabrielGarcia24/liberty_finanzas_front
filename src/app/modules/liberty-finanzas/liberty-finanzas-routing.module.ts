import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LibertyFinanzasComponent } from './liberty-finanzas.component';

const routes: Routes = [

  {
    path: '',
    component: LibertyFinanzasComponent
  },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LibertyFinanzasRoutingModule { }

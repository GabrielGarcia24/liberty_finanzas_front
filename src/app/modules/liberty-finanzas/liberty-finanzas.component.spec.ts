import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LibertyFinanzasComponent } from './liberty-finanzas.component';

describe('LibertyFinanzasComponent', () => {
  let component: LibertyFinanzasComponent;
  let fixture: ComponentFixture<LibertyFinanzasComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LibertyFinanzasComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LibertyFinanzasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

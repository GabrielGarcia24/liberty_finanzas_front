import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LibertyFinanzasRoutingModule } from './liberty-finanzas-routing.module';
import { SharedModule } from '../shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LibertyFinanzasComponent } from './liberty-finanzas.component';


@NgModule({
  declarations: [
    LibertyFinanzasComponent
  ],
  imports: [
    CommonModule,
    LibertyFinanzasRoutingModule,
    SharedModule,    
    FormsModule, 
    ReactiveFormsModule
  ]
})
export class LibertyFinanzasModule { }

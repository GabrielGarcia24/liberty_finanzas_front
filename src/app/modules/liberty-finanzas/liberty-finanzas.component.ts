import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { PrimasServiceService } from 'src/app/services/liberty-finanzas/primas-service.service';

interface componentUISelect {
  value: number;
  viewValue: string;
}

@Component({
  selector: 'app-liberty-finanzas',
  templateUrl: './liberty-finanzas.component.html',
  styleUrls: ['./liberty-finanzas.component.scss']
})
export class LibertyFinanzasComponent implements OnInit {
  
  optionTypeOffice: componentUISelect[] = [
    {value: 1, viewValue: 'Aguas Calientes'},
    {value: 2, viewValue: 'Oaxaca'},
    {value: 3, viewValue: 'Tlaxcala'},
    {value: 4, viewValue: 'Pachuca'}
  ];

  optionTypeAgente: componentUISelect[] = [
    {value: 1, viewValue: 'Agente 1'},
    {value: 2, viewValue: 'Agente 2'},
    {value: 3, viewValue: 'Agente 3'},
    {value: 4, viewValue: 'Agente 4'}
  ];

  optionTypeEjecutiva: componentUISelect[] = [
    {value: 1, viewValue: 'Ejecutiva 1'},
    {value: 2, viewValue: 'Ejecutiva 2'},
    {value: 3, viewValue: 'Ejecutiva 3'},
    {value: 4, viewValue: 'Ejecutiva 4'}
  ];

  displayedColumns: string[] = ['fianza', 'movimiento', 'fiado', 'antiguedad', 'diasVencimiento','importe'];
  dataSource: any;

  selectedValueOffi!: string;
  selectedValueAgent!: string;
  selectedValueEject!: string;
  totalPesos = 0;
  totalEuros = 0;
  totalDolares = 0;

  typeOfficeControl = new FormControl(this.optionTypeOffice[0].value);
  typeAgenteControl = new FormControl(this.optionTypeAgente[0].value);
  typeEjectControl = new FormControl(this.optionTypeEjecutiva[0].value);

  finanzasPrimas!: FormGroup;

  constructor(private formBuilder: FormBuilder, private serviceFinanzas: PrimasServiceService) { }

  ngOnInit(): void {
    this.initForm();
    this.initDataFromFinanzas();
  }
  initDataFromFinanzas() {
    this.serviceFinanzas.getDataFromFinanzas('2').subscribe((dataResponse: any) => {
      this.dataSource = dataResponse.data;
      dataResponse.data.forEach((element: any) => {
        // this.totalPesos = element.importe + this.totalPesos;
      });
      console.log(this.dataSource);
    });
  }

  initForm() {
    this.finanzasPrimas = this.formBuilder.group({
      officeTypeUI: this.typeOfficeControl,
      agenteTypeUI: this.typeAgenteControl,
      ejectTypeUI: this.typeEjectControl
  });
  this.finanzasPrimas.controls.officeTypeUI?.valueChanges.subscribe((value: any) => {
    console.log(value);
    this.selectedValueOffi = value;
    this.serviceFinanzas.getDataFromFinanzas(value).subscribe((dataResponse: any) => {
      this.dataSource = dataResponse.data;
      this.totalPesos = 0;
      this.totalEuros = 0;
      this.totalDolares = 0;
      dataResponse.data.forEach((element: any) => {
        this.totalPesos = element.importe + this.totalPesos;
        this.totalEuros = element.precioEuro + this.totalEuros;
        this.totalDolares = element.precioDolar + this.totalDolares;
      });
      console.log(this.dataSource);
    });
  });
  this.finanzasPrimas.controls.agenteTypeUI?.valueChanges.subscribe((value: any) => {
    console.log(`Agente : ${value}`);
    this.selectedValueAgent = value;
  });
  this.finanzasPrimas.controls.ejectTypeUI?.valueChanges.subscribe((value: any) => {
    console.log(`Ejecutivo : ${value}`);
    this.selectedValueEject = value;
  });  
  }

}
